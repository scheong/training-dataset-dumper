#ifndef JET_CONSTITUENT_WRITER_CONFIG_H
#define JET_CONSTITUENT_WRITER_CONFIG_H

#include <string>
#include <vector>
#include <map>

struct JetConstituentWriterConfig
{
  enum class Type {CUSTOM, CUSTOM_HALF, UCHAR, CHAR, INT, FLOAT, HALF};
  struct Output {
    Type type = Type::CUSTOM;
    std::string name;
  };
  std::string name;
  unsigned size;
  struct OutputBlock {
    std::vector<Output> outputs;
    std::map<std::string, std::string> edm_name;
    bool allow_unused_edm_names;
  };
  OutputBlock constituent;
  std::map<std::string, OutputBlock> associated;
};

#endif
