# Sample from 26th round validation of Sample A, see
#
# https://its.cern.ch/jira/browse/ATR-27040
#
# AODs ttbar with pileup (RoI z-width = 180mm)
valid1.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8453_e8455_s3873_s3874_r14382
# AODs ttbar with pileup (Default RoI Z-width)
valid1.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8453_e8455_s3873_s3874_r14383


# Sample from 24th round validation of Sample A, see
#
# https://its.cern.ch/jira/browse/ATR-26430
#
# AODs ttbar with pileup
# valid1.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8453_e8455_s3873_s3874_r14021

# Sample from 23rd round validation of Sample A, see
#
# https://its.cern.ch/jira/browse/ATR-25880
#
# AODs ttbar with pileup
# valid1.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8453_e8455_s3873_s3874_r13787


# Sample from 22nd round validation of Sample A, see
#
# https://its.cern.ch/jira/browse/ATR-25730
#
# AODs ttbar with pileup
# valid1.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.recon.AOD.e8357_e7400_s3775_r13717_r13718