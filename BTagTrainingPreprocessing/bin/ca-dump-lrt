#!/usr/bin/env python

"""

Setup using setup/athena.sh is required for this script.

This script allows to dump both standard and LRT tracks. 
Two modes are available:
- Dumping merged standard + LRT tracks (use -M)
- Dumping standard and LRT tracks separately (omit -M)

Tracking systematics may also be applied.

"""


from argparse import ArgumentParser
import sys

from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg,
)

from BTagTrainingPreprocessing import trackUtil as trkUtil
from BTagTrainingPreprocessing import dumper


def get_args():
    """
    Extend the base dumper argument parser.
    """
    parser = ArgumentParser(
        formatter_class=dumper.DumperHelpFormatter,
        parents=[dumper.base_parser(__doc__, add_help=False)],
    )
    track_opts = parser.add_mutually_exclusive_group()
    track_opts.add_argument(
        "-t",
        "--track-sys",
        help="list of tracking systematics to apply. For the full list of options\n"
             "see InDetTrackSystematicsTools/InDetTrackSystematics.h",
        nargs="+",
        type=str,
    )
    track_opts.add_argument(
        "-M",
        "--merge-tracks",
        action="store_true",
        help="merge the standard and large d0 track collections before everything else.\n"
             "only works in releases after 22.0.59 (2022-03-08T2101)",
    )
    return parser.parse_args()


def getTrackSysLRTConfig(flags, merge, track_sys):
    """
    This applies track systematics or the LRT merger before 
    re-running track to jet association.
    """

    ca = ComponentAccumulator()

    # first add the track augmentation to define peragee coordinates
    jet_name = 'AntiKt4EMPFlowJets'
    trackContainer = 'InDetTrackParticles'
    primaryVertexContainer = 'PrimaryVertices'
    simpleTrackIpPrefix = 'simpleIp_'
   
    # Run the tracking systematic tools 
    if track_sys:
        ca.merge(trkUtil.applyTrackSys(track_sys, trackContainer, jet_name))
        trackContainer = "InDetTrackParticles_Sys"
    
    # decorate standard tracks with IP info
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            'SimpleTrackAugmenter',
            trackContainer=trackContainer,
            primaryVertexContainer=primaryVertexContainer,
            prefix=simpleTrackIpPrefix,
        )
    )

    # decorate LRT tracks with IP info
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            'SimpleTrackAugmenter_LRTTrack',
            trackContainer="InDetLargeD0TrackParticles",
            primaryVertexContainer=primaryVertexContainer,
            prefix=simpleTrackIpPrefix,
        )
    )

    # merge standard and LRT tracks and associate to the jet
    if merge:
        ca.merge(trkUtil.LRTMerger())
        ca.merge(JetParticleAssociationAlgCfg(
            flags,
            JetCollection=jet_name,
            InputParticleCollection="InDetWithLRTTrackParticles",
            OutputParticleDecoration='JetTagStandardWithLRTTracks',
        ))

    # associate standard and LRT tracks separately
    else:
        ca.merge(JetParticleAssociationAlgCfg(
            flags,
            JetCollection=jet_name,
            InputParticleCollection="InDetTrackParticles",
            OutputParticleDecoration='JetTagStandardTracks',
        ))
        ca.merge(JetParticleAssociationAlgCfg(
            flags,
            JetCollection=jet_name,
            InputParticleCollection="InDetLargeD0TrackParticles",
            OutputParticleDecoration='JetTagLRTTracks',
        ))

    return ca


def run():
    
    args = get_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = getConfig(cfgFlags)
    ca.merge(PoolReadCfg(cfgFlags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    # add track systemtics or LRT merger
    ca.merge(getTrackSysLRTConfig(cfgFlags, args.merge_tracks, args.track_sys))

    # add dumper
    ca.merge(dumper.getDumperConfig(args))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
