#!/usr/bin/env python

"""
Dump a mimimal xAOD to use as an input

Will use a config file to look up e.g. the jet collection.
"""
_extra_truth_help = "Store truth events, jets"
_leptons_help = "Store leptons"

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter as Formatter
import sys, json, re

def get_args():
    parser = ArgumentParser(description=__doc__, formatter_class=Formatter)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-o','--output', default='small_xAOD.pool.root')
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-t','--extra-truth', action='store_true',
                        help=_extra_truth_help)
    parser.add_argument('-l','--leptons', action='store_true',
                        help=_leptons_help)
    mode = parser.add_mutually_exclusive_group(required=True)
    mode.add_argument('-a','--all', action='store_true')
    mode.add_argument('-c','--config-file')
    return parser.parse_args()

def container(typename, key):
    return [
        f'xAOD::{typename}Container#{key}',
        f'xAOD::{typename}AuxContainer#{key}Aux.',
        f'xAOD::AuxContainerBase#{key}Aux.',
    ]
def scalar(typename, key):
    return [
        f'xAOD::{typename}#{key}',
        f'xAOD::{typename}AuxInfo#{key}Aux.',
        f'xAOD::AuxInfoBase#{key}Aux.',
    ]

def get_item_list(config_file, do_truth, do_leptons):
    """
    Build the list of items we want to save, we do this based on the
    configuration file
    """
    # first build the list of items we want
    with open(config_file) as cfg:
        config = json.load(cfg)
    jc = config['jet_collection']
    # we have to do some weird workarounds for the b-tagging nameing,
    # which sometimes removes `Jets` from the name
    bc = 'BTagging_{}'.format(
        jc.rsplit('Jets',1)[0] if jc.endswith('Jets') else jc)
    vc = config['vertex_collection']
    tc = 'InDetTrackParticles'

    # event shapes for jet calibration
    jet_re = re.compile('Anti(.*)Jets')
    shortjet = jet_re.match(jc).group(1)

    item_list = [
        'xAOD::EventInfo#EventInfo',
        'xAOD::EventAuxInfo#EventInfoAux.',
        'xAOD::AuxInfoBase#EventInfoAux.',
        *container('BTagging', bc),
        *container('Jet', jc),
        *container('Vertex', vc),
        *container('TrackParticle', tc),
        *container('TruthParticle','TruthParticles'),
        # according to TJ PUSB might be needed for "modern"
        # calibrations
        *scalar('EventShape', f'{shortjet}EventShape'),
        *scalar('EventShape', f'{shortjet}PUSBEventShape'),
    ]
    if do_truth:
        item_list += [
            *container('TruthEvent', 'TruthEvents'),
            *container('Jet', 'AntiKt4TruthJets'),
        ]
    if do_leptons:
        item_list += [
            *container('Muon','Muons'),
            *container('Electron','Electrons'),
        ]
    return item_list

def not_none(itemlist):
    return [i for i in itemlist if not i.startswith('None')]

def run():
    args = get_args()

    if not args.all:
        item_list = get_item_list(
            args.config_file,
            args.extra_truth,
            args.leptons
        )
        sys.stdout.write('Item list:\n')
        for item in item_list:
            sys.stdout.write(f' {item}\n')

    cfgFlags.Input.Files = args.input_files
    if args.all:
        item_list = not_none(cfgFlags.Input.TypedCollections)
    cfgFlags.Output.AODFileName = args.output
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events
    cfgFlags.lock()

    ca = getConfig(cfgFlags)
    ca.merge(PoolReadCfg(cfgFlags))
    ca.merge(OutputStreamCfg(cfgFlags, "AOD", ItemList=item_list))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
