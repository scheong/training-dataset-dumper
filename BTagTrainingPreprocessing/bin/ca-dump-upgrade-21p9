#!/usr/bin/env python

"""
This dumps datasets for upgrade studies

The primary difference between this and ca-dump-single-btag is that
upgrade is still working in release 21. To support release 21 AODs in
release 22, we have to augment the data in a few ways.
"""


from BTagTrainingPreprocessing import dumper

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from DerivationFrameworkMCTruth.TruthDerivationToolsConfig import DFCommonTruthClassificationToolCfg

import sys


def getUpgradeConfig(cfgFlags, args):

    ca = ComponentAccumulator()

    # decorate detailed track truth information
    from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConfig import InDetTrackTruthOriginToolCfg
    trackTruthOriginTool = ca.popToolsAndMerge(InDetTrackTruthOriginToolCfg(cfgFlags))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
        'TruthParticleDecoratorAlg',
        trackTruthOriginTool=trackTruthOriginTool
    ))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
        'TrackTruthDecoratorAlg', trackContainer='InDetTrackParticles', trackTruthOriginTool=trackTruthOriginTool))

    # decorate with promptLepton information
    DFCommonTruthClassifier = ca.getPrimaryAndMerge(DFCommonTruthClassificationToolCfg(cfgFlags))
    ca.addEventAlgo(CompFactory.DerivationFramework.CommonAugmentation(
        'promptLeptonAugmenter', AugmentationTools = [DFCommonTruthClassifier]))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        'PoorMansIpAugmenterAlg', prefix='btagIp_', primaryVertexContainer='PrimaryVertices'))

    btag_name = 'BTagging_AntiKt4EMTopo'
    jet_name = 'AntiKt4EMTopoJets'
    ca.addEventAlgo(CompFactory.BTagToJetLinkerAlg(
        'jetToBTag',
        newLink=f'{btag_name}.jetLink',
        oldLink=f'{jet_name}.btaggingLink'
    ))

    ca.merge(dumper.getDumperConfig(args))

    return ca


def run():
    args = dumper.base_parser(__doc__).parse_args()
    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.Digitization.PileUp = True # Need to be tweaked by hand for 21.9 files with pile-up
    cfgFlags.lock()

    ca = getConfig(cfgFlags)

    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(getUpgradeConfig(cfgFlags, args))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)



