from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def getMCTC():
    """
    Mergers an alg which decorate the "TruthParticles" container with 
    MCTC particle origin and type.
    """
    
    truthClassifier = CompFactory.MCTruthClassifier(
        name="DumperMCTruthClassifier",
        xAODTruthParticleContainerName="TruthParticles",
    )

    truthClassifierAlg = CompFactory.MCTCDecoratorAlg(
        name="MCTCDecoratorAlg",
        MCTruthClassifier=truthClassifier,
        truthContainer="TruthParticles",
    )
    
    ca = ComponentAccumulator()
    ca.addEventAlgo(truthClassifierAlg)

    return ca
