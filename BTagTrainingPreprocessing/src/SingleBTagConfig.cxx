#include "SingleBTagConfig.hh"
#include "ConfigFileTools.hh"

#include <boost/optional/optional.hpp>

#include <filesystem>
#include <set>
// set up some nlohmann converters from json, used by nlohmann::get<T>()
NLOHMANN_JSON_SERIALIZE_ENUM( DL2Config::Where, {
    {DL2Config::Where::UNKNOWN, ""},
    {DL2Config::Where::JET, "jet"},
    {DL2Config::Where::BTAG, "btag"},
})
NLOHMANN_JSON_SERIALIZE_ENUM( DL2Config::Engine, {
    {DL2Config::Engine::UNKNOWN, ""},
    {DL2Config::Engine::DL2, "dl2"},
    {DL2Config::Engine::GNN, "gnn"},
    {DL2Config::Engine::XBB, "xbb"},
})

NLOHMANN_JSON_SERIALIZE_ENUM( JetLinkWriterConfig::Type, {
    {JetLinkWriterConfig::Type::UNKNOWN, ""},
    {JetLinkWriterConfig::Type::FlowElement, "flow"},
    {JetLinkWriterConfig::Type::IParticle, "iparticle"},
    {JetLinkWriterConfig::Type::CaloCluster, "calocluster"},
})

NLOHMANN_JSON_SERIALIZE_ENUM( HitConfig::Origin, {
    {HitConfig::Origin::UNKNOWN, ""},
    {HitConfig::Origin::PrimaryVertex, "primary_vertex"},
    {HitConfig::Origin::Beamspot, "beamspot"}
})

namespace {

  const std::string g_ip_prefix = "ip_prefix";

  TrackSortOrder get_track_sort_order(const nlohmann::ordered_json& nlocfg,
                                const std::string& key) {
    std::string val = nlocfg.at(key).get<std::string>();
    if (val == "abs_d0_significance") {
      return TrackSortOrder::ABS_D0_SIGNIFICANCE;
    }
    if (val == "abs_d0") {
      return TrackSortOrder::ABS_D0;
    }
    if (val == "d0_significance") {
      return TrackSortOrder::D0_SIGNIFICANCE;
    }
    if (val == "abs_beamspot_d0") {
      return TrackSortOrder::ABS_BEAMSPOT_D0;
    }
    if (val == "pt") {
      return TrackSortOrder::PT;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }

  std::vector<JetConstituentWriterConfig::Output> get_constituent_variables(
    const nlohmann::ordered_json& nlocfg)
  {
    using Type = JetConstituentWriterConfig::Type;
    using Op = JetConstituentWriterConfig::Output;
    namespace cf = ConfigFileTools;
    std::vector<JetConstituentWriterConfig::Output> vars;
    std::set<std::string> keys = cf::get_key_set(nlocfg);

    auto fill = [&vars, &keys, &nlocfg](const std::string& name, Type type) {
      if (keys.erase(name)) {
        for (auto v: cf::get_list(nlocfg[name])) {
          vars.emplace_back<Op>({type, v});
        }
      }
    };
    fill("uchars", Type::UCHAR);
    fill("chars", Type::CHAR);
    fill("ints", Type::INT);
    fill("halves", Type::HALF);
    fill("floats", Type::FLOAT);
    fill("customs", Type::CUSTOM);
    fill("half_precision_customs", Type::CUSTOM_HALF);
    cf::throw_if_any_keys_left(keys, "track variable types");
    return vars;
  }

  auto get_constituent_outblock_config(const nlohmann::ordered_json& nlocfg)
  {
    const std::string edm_block_name = "edm_names";
    JetConstituentWriterConfig::OutputBlock cfg;
    cfg.outputs = get_constituent_variables(nlocfg["variables"]);
    if (nlocfg.count(edm_block_name)) {
      for (const auto& [outname, edmname]: nlocfg.at("edm_names").items()) {
        cfg.edm_name[outname] = edmname.get<std::string>();
      }
    }
    cfg.allow_unused_edm_names = false;
    if (nlocfg.count("allow_unused_edm_names")) {
      cfg.allow_unused_edm_names = nlocfg.at("allow_unused_edm_names").get<bool>();
    }
    return cfg;
  }

  auto get_constituent_config(const nlohmann::ordered_json& nlocfg) {
    const std::string association_block = "associations";
    JetConstituentWriterConfig writer;
    writer.constituent = get_constituent_outblock_config(nlocfg);
    writer.name = nlocfg.at("output_name").get<std::string>();
    writer.size = nlocfg.at("n_to_save").get<size_t>();
    if (nlocfg.count(association_block)) {
      for (const auto& [type, block]: nlocfg.at(association_block).items()) {
        writer.associated[type] = get_constituent_outblock_config(block);
      }
    }
    return writer;
  }

  auto get_jet_link_config(const nlohmann::ordered_json& nlocfg) {
    JetLinkWriterConfig cfg;
    using Type = JetLinkWriterConfig::Type;
    auto type = nlocfg.at("type").get<Type>();
    if (type == Type::UNKNOWN) {
      throw std::runtime_error("unknown flow constituents type");
    }
    cfg.constituents = get_constituent_config(nlocfg);
    cfg.accessor = "constituentLinks";
    cfg.type = type;
    return cfg;
  }

  TrackConfig get_track_config(const nlohmann::ordered_json& nlocfg) {
    namespace cft = ConfigFileTools;
    TrackConfig cfg;

    cfg.sort_order = get_track_sort_order(nlocfg,"sort_order");

    const nlohmann::ordered_json& tracksel = nlocfg.at("selection");
    TrackSelectorConfig::Cuts& cuts = cfg.selection.cuts;
    cuts.pt_minimum        = cft::null_as_nan(tracksel,"pt_minimum");
    cuts.abs_eta_maximum   = cft::null_as_nan(tracksel,"abs_eta_maximum");
    cuts.d0_maximum        = cft::null_as_nan(tracksel,"d0_maximum");
    cuts.z0_maximum        = cft::null_as_nan(tracksel,"z0_maximum");
    cuts.si_hits_minimum   = tracksel.at("si_hits_minimum").get<int>();
    cuts.si_shared_maximum = tracksel.at("si_shared_maximum").get<int>();
    cuts.si_holes_maximum  = tracksel.at("si_holes_maximum").get<int>();
    cuts.pix_holes_maximum = tracksel.at("pix_holes_maximum").get<int>();

    cfg.selection.btagging_link = nlocfg.at("btagging_link").get<std::string>();

    cfg.writer = get_constituent_config(nlocfg);
    cfg.input_name = nlocfg.at("input_name").get<std::string>();

    // use the same prefix for selection and everything else
    std::string ip_prefix = nlocfg.at(g_ip_prefix).get<std::string>();
    if (cfg.writer.constituent.edm_name.count(g_ip_prefix)) {
      throw std::runtime_error(
        "you should not specify '" + g_ip_prefix
        + "' in track edm remapping");
    }
    cfg.writer.constituent.edm_name[g_ip_prefix] = ip_prefix;
    cfg.selection.ip_prefix = ip_prefix;

    return cfg;
  }

  SoftElectronConfig get_electron_config(const nlohmann::ordered_json& nlocfg) {
    namespace cft = ConfigFileTools;
    SoftElectronConfig cfg;
    const nlohmann::ordered_json& elsel = nlocfg.at("selection");
    SoftElectronSelectorConfig::Cuts& cuts = cfg.selection.cuts;
    cuts.pt_minimum        = cft::null_as_nan(elsel,"pt_minimum");
    cuts.pt_maximum        = cft::null_as_nan(elsel,"pt_maximum");
    cuts.abs_eta_maximum   = cft::null_as_nan(elsel,"abs_eta_maximum");
    cuts.d0_maximum        = cft::null_as_nan(elsel,"d0_maximum");
    cuts.eta_maximum       = cft::null_as_nan(elsel,"eta_maximum");
    cuts.ptrel_maximum     = cft::null_as_nan(elsel,"ptrel_maximum");
    cuts.isopt_maximum     = cft::null_as_nan(elsel,"isopt_maximum");
    cuts.eop_maximum       = cft::null_as_nan(elsel,"eop_maximum");
    cuts.rhad1_maximum     = cft::null_as_nan(elsel,"rhad1_maximum");
    cuts.wstot_maximum     = cft::null_as_nan(elsel,"wstot_maximum");
    cuts.rphi_maximum      = cft::null_as_nan(elsel,"rphi_maximum");
    cuts.reta_maximum      = cft::null_as_nan(elsel,"reta_maximum");
    cuts.deta1_maximum     = cft::null_as_nan(elsel,"deta1_maximum");
    cuts.dpop_maximum      = cft::null_as_nan(elsel,"dpop_maximum");

    JetLinkWriterConfig writer;
    writer.constituents = get_constituent_config(nlocfg);
    writer.type = JetLinkWriterConfig::Type::Electron;
    writer.accessor = "jet_electrons";
    cfg.writer = writer;

    return cfg;
  }

  HitConfig get_hit_config(const nlohmann::ordered_json& raw_cfg) {
    namespace cft = ConfigFileTools;
    HitWriterConfig writer;
    ConfigFileTools::OptionalConfigurationObject cfg(raw_cfg);

    writer.output_size = cfg.require<size_t>("output_size");
    writer.name = cfg.require<std::string>("output_name");
    writer.dist_to_jet = cfg.require<float>("distance_to_jet");
    writer.save_endcap_hits = cfg.get<bool>("save_endcap_hits", false);
    writer.save_only_clean_hits = cfg.get<bool>("save_only_clean_hits", false);
    writer.save_sct = cfg.get<bool>("save_sct", false);
    writer.variables = cfg.require<std::vector<std::string>>("variables");

    HitDecoratorConfig decorator;
    decorator.dR_hit_to_jet = writer.dist_to_jet;
    decorator.use_splitProbability = cfg.get<bool>("use_splitProbability", false);
    decorator.save_endcap_hits = writer.save_endcap_hits;

    std::string container = cfg.require<std::string>("container_name");
    HitConfig::Origin origin = cfg.require<HitConfig::Origin>("origin");

    cfg.throw_if_unused("hits");

    return {container, writer, decorator, origin};
  }

  TrackSortOrder get_truth_sort_order(const std::string& val) {
    if (val == "pt") {
      return TrackSortOrder::PT;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }

  TruthSelectorConfig::Particle get_truth_particle(const std::string& name)
  {
    using p = TruthSelectorConfig::Particle;
    if (name == "truth_hadrons") return p::hadron;
    if (name == "truth_leptons") return p::lepton;
    if (name == "truth_fromBC") return p::fromBC;
#define TRY(KEY) if (name == #KEY) return p::KEY
    TRY(overlapLepton);
    TRY(promptLepton);
    TRY(nonPromptLepton);
    TRY(muon);
    TRY(stableNonGeant);
    TRY(higgs);
    TRY(top);
#undef TRY
    throw std::logic_error("unknown truth particle type: " + name);
  }

  TruthConfig get_truth_config(const nlohmann::ordered_json& js) {
    TruthConfig cfg;

    using Opts = ConfigFileTools::OptionalConfigurationObject;
    Opts json(js);

    // the configuration can have either a selection object or a merge
    // list, not both
    Opts association(json.get("association"));
    const nlohmann::ordered_json& merge = json.get("merge");
    if (!association.empty() && !merge.empty()) {
      throw std::runtime_error(
        "Truth config can't contain both association and merging");
    }

    std::string particles = "";
    if (!association.empty()) {
      TruthSelectorConfig tsc;
      particles = association.require<std::string>("particles");
      tsc.particle = get_truth_particle(particles);
      const auto& containers = association.get<std::vector<std::string>>("containers", {});
      if (!containers.empty()) {
        tsc.containers = containers;
      } else {
        tsc.containers = {association.get("container", "TruthParticles")};
      }
      auto& kin = tsc.kinematics;
      kin.pt_minimum = association.get("pt_minimum", 0.0f);
      kin.abs_eta_maximum = association.get("abs_eta_maximum", INFINITY);
      kin.dr_maximum = association.require<float>("dr_maximum");
      cfg.selection = tsc;
    }
    association.throw_if_unused("selection");

    if (!merge.empty()) {
      cfg.merge = merge.get<std::vector<std::string>>();
    }

    cfg.overlap_dr = json.get("overlap_dr", 0.0);

    // use association name if specified, otherwise reuse particles name
    std::string association_name = json.get("association_name", "");
    if (not association_name.empty()) {
      cfg.association_name = association_name;
    }
    else if (not particles.empty()) {
      cfg.association_name = particles;
    }
    else {
      throw std::runtime_error(
        "If not specifying \"association_name\" you need to specify some"
        " \"particles\" to associate");
    }

    Opts output(json.get("output"));
    if (!output.empty()) {
      TruthOutputConfig out;
      out.n_to_save = output.require<int>("n_to_save");
      out.sort_order = get_truth_sort_order(
        output.require<std::string>("sort_order"));
      out.name = output.get("name", cfg.association_name);
      cfg.output = out;
    }
    output.throw_if_unused("output");

    cfg.decorate = json.get("decorate_summary", false);

    json.throw_if_unused("truth config");

    return cfg;
  }

  DecorateConfig get_decoration_config(const nlohmann::ordered_json& raw) {
    namespace cft = ConfigFileTools;
    DecorateConfig cfg;

    // the decorators will default to false, but we also make sure
    // that every decorator specified in the configuration is used.
    ConfigFileTools::OptionalConfigurationObject nlocfg(raw);
#define FILL(field) cfg.field = nlocfg.get(#field, false)
    FILL(jet_aug);
    FILL(btag_jes);
    FILL(soft_muon);
    FILL(track_sv_info);
    FILL(do_vrtrackjets_fix);
    FILL(do_heavyions);
    FILL(lepton_decay_label);
    FILL(truth_pileup);
#undef FILL
    nlocfg.throw_if_unused("decorations");
    return cfg;
  }

  JetCalibrationConfig get_jet_calibration(
    const nlohmann::ordered_json& nlocfg)
  {
    JetCalibrationConfig config;
#define FILL(field) config.field = nlocfg.at(#field).get<std::string>()
    FILL(collection);
    FILL(configuration);
    FILL(seq);
    FILL(area);
#undef FILL
    return config;
  }

  SelectionConfig get_selection_config(const nlohmann::ordered_json& raw) {
    SelectionConfig cfg;
    ConfigFileTools::OptionalConfigurationObject nlocfg(raw);
    // make sure the type of `defval` matches the type of `field`
#define FILL(field, defval) cfg.field = nlocfg.get(#field, defval)
    FILL(truth_jet_matching, false);
    FILL(truth_jet_collection, "AntiKt4TruthDressedWZJets");
    FILL(truth_primary_vertex_matching, false);
    FILL(minimum_jet_constituents, 0);
    FILL(minimum_jet_pt, 0.0);
    FILL(maximum_jet_pt, INFINITY);
    FILL(maximum_jet_absolute_eta, INFINITY);
    FILL(minimum_jet_mass, 0.0);
    FILL(maximum_jet_mass, INFINITY);
    FILL(minimum_jvt, -INFINITY);
#undef FILL
    // jet cleaning
    std::string jet_cleaning = nlocfg.get("jet_cleaning","none");
    const std::map<std::string, JetCleanOption> jetclean_option_map = {
      {"none", JetCleanOption::none},
      {"event", JetCleanOption::event},
      {"jet", JetCleanOption::jet} };
    if (!jetclean_option_map.count(jet_cleaning)) {
      std::string problem = "unknown jet cleaning '" + jet_cleaning +
        "', pick one of the following";
      std::string sep = ": ";
      for (const auto& itr: jetclean_option_map) {
        problem.append(sep + "'" + itr.first + "'");
        sep = ", ";
      }
      throw std::runtime_error(problem);
    }
    cfg.jet_cleaning = jetclean_option_map.at(jet_cleaning);

    nlocfg.throw_if_unused("selection options");
    return cfg;
  }

  SubjetConfig get_subjet_config(const nlohmann::ordered_json& nlocfg) {
    namespace cft = ConfigFileTools;
    SubjetConfig cfg;
    cfg.input_name = nlocfg.at("input_name").get<std::string>();
    cfg.output_name = nlocfg.at("output_name").get<std::string>();
    cfg.n_subjets_to_save = nlocfg.at("n_subjets_to_save").get<size_t>();
    cfg.min_jet_pt = nlocfg.at("min_jet_pt").get<double>();
    cfg.max_abs_eta = nlocfg.at("max_abs_eta").get<double>();
    cfg.num_const = nlocfg.at("num_const").get<size_t>();
    cfg.btagging_link = nlocfg.at("btagging_link").get<std::string>();
    cfg.tracks_name = nlocfg.at("tracks_name").get<std::string>();
    // read in the b-tagging variables
    const auto& nlo_variables = nlocfg["variables"];
    if(nlo_variables.contains("btagging")) {
      from_json(nlo_variables["btagging"], cfg.btagging_vars);
    }
    if(nlo_variables.contains("jet")) {
      from_json(nlo_variables["jet"], cfg.jet_vars);
    }
    return cfg;
  }

  // do some consistency checks
  void requireBTag(bool variable, const std::string& varname,
                   const std::string& btagging_link) {
    if (variable && btagging_link.empty()) {
      throw std::runtime_error(varname + " requires btagging_link to be set");
    }
  }
  void checkConsistency(const SingleBTagConfig& cfg) {
    const std::string& btagname = cfg.btagging_link;
    const DecorateConfig& dec = cfg.decorate;
#define REQUIRES_BTAG(name) requireBTag(dec.name, #name, btagname)
    REQUIRES_BTAG(btag_jes);
    REQUIRES_BTAG(soft_muon);
    REQUIRES_BTAG(jet_aug);
    #undef REQUIRES_BTAG
    const auto& dl2s = cfg.dl2_configs;
    int n_btags = std::count_if(
      dl2s.begin(), dl2s.end(),
      [](auto& x) { return x.where == DL2Config::Where::BTAG; });
    requireBTag(n_btags > 0, "applying btags", btagname);

    if (cfg.vertex_collection.empty()) {
      if (cfg.selection.truth_primary_vertex_matching) {
        throw std::runtime_error(
          "truth primary vertex matching requires primary vertex");
      }
      if (cfg.hits) {
        throw std::runtime_error("hits variables require primary vertex");
      }
    }
  }

  DL2Config get_dl2_config(const nlohmann::ordered_json& node) {

    // keeps track of used keys
    ConfigFileTools::OptionalConfigurationObject nlocfg(node);

    using Where = DL2Config::Where;
    auto where = nlocfg.get<Where>("where", Where::BTAG);
    if (where == Where::UNKNOWN) {
      throw std::runtime_error("unknown tagging target");
    }

    using Engine = DL2Config::Engine;
    auto engine = nlocfg.get<Engine>("engine", Engine::DL2);
    if (engine == Engine::UNKNOWN) throw std::runtime_error(
      "unknown tagging engine");

    // get the variable remapping
    const auto& remap_obj = nlocfg.get("remapping", nlohmann::json::object());
    const auto& remapping = remap_obj.get<std::map<std::string, std::string>>();

    // get the file path
    std::string nn_file_path = nlocfg.get<std::string>("nn_file_path", "");
    if (nn_file_path.empty()) throw std::runtime_error("No nn_file_path found in DL2 config");

    // get the flip tag config
    const auto& flip_tag_config = FlavorTagDiscriminants::flipTagConfigFromString(
      nlocfg.get("flip_tag_config", "STANDARD")
    );

    // check we used everything and return
    nlocfg.throw_if_unused("DL2 key");
    return {where, engine, nn_file_path, remapping, flip_tag_config};
  }

}

const nlohmann::ordered_json get_merged_json(const std::filesystem::path& cfg_path)
{
  namespace fs = std::filesystem;
  namespace cft = ConfigFileTools;
  if (!fs::exists(cfg_path)) {
    throw std::runtime_error(cfg_path.string() + " doesn't exist");
  }
  std::ifstream cfg_stream(cfg_path);
  auto nlocfg = nlohmann::ordered_json::parse(cfg_stream);
  cft::combine_files(nlocfg, cfg_path.parent_path());
  return nlocfg;
}

SingleBTagConfig get_singlebtag_config(const std::filesystem::path& cfg_path)
{
  auto nlocfg = get_merged_json(cfg_path);
  return get_singlebtag_config(nlocfg, cfg_path.stem());
}



SingleBTagConfig get_singlebtag_config(const nlohmann::ordered_json& nlocfg,
                                       const std::string& tool_prefix)
{
  namespace cft = ConfigFileTools;
  using OJ = nlohmann::ordered_json;
  using VOJ = std::vector<OJ>;

  SingleBTagConfig config;
  config.tool_prefix = tool_prefix;

  ConfigFileTools::OptionalConfigurationObject opt_nlocfg(nlocfg);

  config.selection = get_selection_config(opt_nlocfg.get("selection"));

  config.jet_collection = opt_nlocfg.require<std::string>("jet_collection");
  config.is21p9_AOD = opt_nlocfg.get("is21p9_AOD", false);

  const auto& jet_calib = opt_nlocfg.require<OJ>("calibration");
  if (!jet_calib.empty()) {
    config.calibration = get_jet_calibration(jet_calib);
  }

  if (config.selection.jet_cleaning == JetCleanOption::jet &&
      config.jet_collection == "AntiKt4EMPFlowJets") {
    throw std::runtime_error(
      "Must not use individual jet cleaning for " + config.jet_collection +
      ". Use {\"jet_cleaning\": \"event\"} instead.");
  }

  config.vertex_collection = opt_nlocfg.get("vertex_collection", "");
  config.btagging_link = opt_nlocfg.get("btagging_link","");

  for (const auto& trkpt: opt_nlocfg.require<VOJ>("tracks")) {
    config.tracks.push_back(get_track_config(trkpt));
  }

  // electrons config
  const auto& electrons = opt_nlocfg.get("electrons");
  if (!electrons.empty()) {
    config.electrons = get_electron_config(electrons);
  }

  config.nntc = opt_nlocfg.get<std::string>("nntc","");

  // The hits block should exist for trackless studies
  const auto& hits = opt_nlocfg.get("hits");
  if (!hits.empty()) {
    config.hits = get_hit_config(hits);
  }

  for (const auto& trthpt: opt_nlocfg.get<VOJ>("truths", {})) {
    config.truths.push_back(get_truth_config(trthpt));
  }

  for (const auto& nn_cfg: opt_nlocfg.get<VOJ>("dl2_configs", {})) {
    config.dl2_configs.push_back(get_dl2_config(nn_cfg));
  }

  // subjet config
  for (const auto& subjet: opt_nlocfg.get<VOJ>("subjets", {})) {
    config.subjet_configs.push_back(get_subjet_config(subjet));
  }

  // pflow constituents
  const auto& flow = opt_nlocfg.get("flow");
  if (!flow.empty()) {
    config.flow = get_jet_link_config(flow);
  }

  const auto& nlo_variables = opt_nlocfg.require<OJ>("variables");
  if(nlo_variables.contains("btagging")) {
    from_json(nlo_variables["btagging"], config.btagging_vars);
  }
  if(nlo_variables.contains("jet")) {
    from_json(nlo_variables["jet"], config.jet_vars);
  }
  if(nlo_variables.contains("event")) {
    from_json(nlo_variables["event"], config.event_vars);
  }
  // config.btagging_vars = cft::get_variable_list(nlo_variables["btagging"]);
  // config.jet_vars = cft::get_variable_list(nlo_variables["jet"]);
  // config.event_vars = cft::get_variable_list(nlo_variables["event"]);
  config.default_flag_mapping = cft::check_map_from(
    nlo_variables["default_mapping"]);

  config.decorate = get_decoration_config(opt_nlocfg.get("decorate"));

  opt_nlocfg.throw_if_unused("top level options");

  checkConsistency(config);

  return config;
}

void force_full_precision(JetConstituentWriterConfig::OutputBlock& block) {
  using Type = JetConstituentWriterConfig::Type;
  for (auto& output: block.outputs) {
    if (output.type == Type::HALF) output.type = Type::FLOAT;
    if (output.type == Type::CUSTOM_HALF) output.type = Type::CUSTOM;
  }
}

void force_full_precision(SingleBTagConfig& cfg) {
  cfg.force_full_precision = true;
  for (auto& trk: cfg.tracks) {
    force_full_precision(trk.writer.constituent);
    for (auto& [_, block]: trk.writer.associated) {
      force_full_precision(block);
    }
  }
}
