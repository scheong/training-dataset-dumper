#include "BTagElectronAugmenter.hh"
#include <set>
#include "xAODBTagging/BTaggingUtilities.h"

// the constructor just builds the decorator
BTagElectronAugmenter::BTagElectronAugmenter(const BTagElectronAugmenter::BLA& btag_link):
  m_btagging_link(btag_link),
  // isdefault; total 19 electron vars 
  m_dec_el_isdefault_wp3("softElectron_isDefaults"),
  // kinematic var  
  m_dec_el_pt_r_wp3("softElectron_ptr"),
  m_dec_el_dr_wp3("softElectron_dr"),
  m_dec_el_abseta_wp3("softElectron_abseta"),
  // tracking var 
  m_dec_el_d0_signed_wp3("softElectron_d0Signed"),
  m_dec_el_z0_signed_wp3("softElectron_z0Signed"),
  m_dec_el_d0_signed_sig_wp3("softElectron_d0SignedSig"),
  // additional var 
  m_dec_el_iso_pt_wp3("softElectron_isoPt"),
  m_dec_el_phf_wp3("softElectron_phf"),
  // tracking from dnn
  m_dec_el_epht_wp3("softElectron_epht"),
  // track cluster 
  m_dec_el_dphires_wp3("softElectron_dphires"),
  m_dec_el_eop_wp3("softElectron_eop"),
  // cal var  
  m_dec_el_rhad_wp3("softElectron_rhad"),
  m_dec_el_rhad1_wp3("softElectron_rhad1"),
  m_dec_el_eratio_wp3("softElectron_eratio"),
  m_dec_el_weta2_wp3("softElectron_weta2"),
  m_dec_el_reta_wp3("softElectron_reta"),
  m_dec_el_f1_wp3("softElectron_f1"),
  m_dec_el_f3_wp3("softElectron_f3")
{
}

// this call actually does the work on the jet
void BTagElectronAugmenter::augment(const xAOD::Jet& jet, const xAOD::ElectronContainer& electrons, 
                                    const xAOD::Vertex& pv, const EventContext& ctx, const AsgElectronSelectorTool& electron_select) const {
  // it's good practice to check that the b-tagging object exists. It
  // _should_ in most cases, but if not better to avoid the segfault.
  const xAOD::BTagging* btag = *m_btagging_link(jet);
  if (!btag) {
    throw std::runtime_error("no b-tagging object on jet");
  }

  // initiate vars to be added 
  unsigned int el_index_wp3 = 99; // index of the loose electron selection (above wp3)

  float target_el_dr = 0.4;
  float max_el_phf = 0.0;
  
  TLorentzVector jet_4vec;
  jet_4vec.SetPtEtaPhiE(jet.pt(), jet.eta(), jet.phi(), jet.e());

  // add the closest electron and the highest PHF electron 
  for (unsigned int iel = 0; iel < electrons.size(); iel++){
    const auto& el = electrons.at( iel );
    TLorentzVector el_4vec;
    el_4vec.SetPtEtaPhiE(el->pt(), el->eta(), el->phi(), el->e());
    float dr_j_e = jet_4vec.DeltaR(el_4vec);
    float phf_select = electron_select.calculateMultipleOutputs(ctx, el).at(3); 

    // for highest phf electron: add dr
    if((dr_j_e < target_el_dr) & (phf_select > max_el_phf)){
      el_index_wp3 = iel;
      max_el_phf = phf_select; 
    }
  }

  // for highest phf electron: add other info 
  update_var(jet, electrons, pv, jet_4vec, el_index_wp3, "cut3", max_el_phf);
}

void BTagElectronAugmenter::update_var(const xAOD::Jet& jet, const xAOD::ElectronContainer& electrons, 
                                       const xAOD::Vertex& pv, const TLorentzVector jet_4vec, 
                                       const int el_index, const std::string el_wp, const float max_phf) const {
  // add 19 electron variables 
  char el_isdefault = 1; 
  // kinematic var (4+2)
  float el_pt_r = -1;
  float el_dr = -1;
  float el_abseta = -1; 
  // tracking var (x4)
  float el_d0_signed = -1; 
  float el_z0_signed = -1; 
  float el_d0_signed_sig = -1;
  // additional var (x2)
  float el_iso_pt = -1; 
  float el_phf = -1; 
  // tracking dnn (x4)
  float el_epht = -1;
  // track cluster dnn (x3)
  float el_eop = -1; 
  float el_dphires = -1; 
  // cal var dnn (x9) 
  float el_eratio = -1; 
  float el_rhad = -1;
  float el_rhad1 = -1;
  float el_weta2 = -1;
  float el_reta = -1;
  float el_f1 = -1;
  float el_f3 = -1;

  if (el_index < 99){
    const auto& el = electrons.at(el_index); 
    el_isdefault = 0; 
    // kinematic vars (4+2)
    float el_pt = el->pt(); 
    el_pt_r = el_pt / jet.pt();
    TLorentzVector el_4vec;
    el_4vec.SetPtEtaPhiE(el->pt(), el->eta(), el->phi(), el->e());
    el_dr = jet_4vec.DeltaR(el_4vec);
    el_abseta = std::abs(el->eta());
    auto track = el->trackParticle();

    // tracking vars (x4)
    float el_d0 = track->d0();
    double vs = std::sin( jet.phi() - track->phi() )*el_d0;
    double sign = (vs >= 0) ? 1 : -1; 
    el_d0_signed = sign * abs(el_d0); 

    float el_z0 = track->z0() + (track->vz() - pv.z()); 
    double vs_z = (jet.eta() - track->eta()) *el_z0;
    double sign_z = (vs_z >= 0) ? 1 : -1;
    el_z0_signed = sign_z * abs(el_z0); 

    const auto cov_matrix_d0 = track->definingParametersCovMatrix();
    el_d0_signed_sig = el_d0_signed / sqrt(cov_matrix_d0(0, 0));

    // additional vars (x2)
    el_iso_pt = el->auxdata<float>("ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000") / el_pt;
    el_phf = max_phf;

    // track cluster (x3)
    float one_over_p =std::abs(track->qOverP());
    // Ratio of the cluster energy to the track momentum
    float energy = el->caloCluster()->e();
    el_eop = energy * one_over_p;
    el->trackCaloMatchValue(el_dphires, xAOD::EgammaParameters::deltaPhiRescaled2); 

    // cal vars (x9)
    el->showerShapeValue(el_rhad1, xAOD::EgammaParameters::Rhad1);
    el->showerShapeValue(el_rhad, xAOD::EgammaParameters::Rhad); 
    el->showerShapeValue(el_eratio, xAOD::EgammaParameters::Eratio);
    el->showerShapeValue(el_weta2, xAOD::EgammaParameters::weta2);
    el->showerShapeValue(el_reta, xAOD::EgammaParameters::Reta); 
    el->showerShapeValue(el_f1, xAOD::EgammaParameters::f1);
    el->showerShapeValue(el_f3, xAOD::EgammaParameters::f3);
    if (el_abseta > 2.01) {
      el_f3 = 0.05;
    }
  }

  const xAOD::BTagging* btag = *m_btagging_link(jet);
  if (el_wp=="cut3") {
    // 19 softe vars 
    m_dec_el_isdefault_wp3(*btag) = el_isdefault;
    // kinematic var (4+2)
    m_dec_el_pt_r_wp3(*btag) = el_pt_r;
    m_dec_el_dr_wp3(*btag) = el_dr;
    m_dec_el_abseta_wp3(*btag) = el_abseta;
    // track var (x4)
    m_dec_el_d0_signed_wp3(*btag) = el_d0_signed;
    m_dec_el_z0_signed_wp3(*btag) = el_z0_signed;
    m_dec_el_d0_signed_sig_wp3(*btag) = el_d0_signed_sig; 
    // additional var (x2)
    m_dec_el_iso_pt_wp3(*btag) = el_iso_pt;
    m_dec_el_phf_wp3(*btag) = el_phf;
    // dnn tracking (x4)
    m_dec_el_epht_wp3(*btag) = el_epht;
    // track cluster (x3)
    m_dec_el_dphires_wp3(*btag) = el_dphires;
    m_dec_el_eop_wp3(*btag) = el_eop;
    // dnn cal (x9)
    m_dec_el_rhad_wp3(*btag) = el_rhad;
    m_dec_el_rhad1_wp3(*btag) = el_rhad1;
    m_dec_el_eratio_wp3(*btag) = el_eratio;
    m_dec_el_weta2_wp3(*btag) = el_weta2;
    m_dec_el_reta_wp3(*btag) = el_reta;
    m_dec_el_f1_wp3(*btag) = el_f1;
    m_dec_el_f3_wp3(*btag) = el_f3;
  }
}
