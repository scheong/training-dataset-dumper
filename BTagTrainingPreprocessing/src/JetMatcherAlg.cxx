/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "JetMatcherAlg.h"
#include "AsgMessaging/MessageCheck.h"

#include <memory>


JetMatcherAlg::JetMatcherAlg(const std::string& name,
                                         ISvcLocator* pSvcLocator):
  AthReentrantAlgorithm(name, pSvcLocator)
{
  declareProperty("sourceJets", m_sourceJets);
  declareProperty("floatsToCopy", m_floats.toCopy);
  declareProperty("intsToCopy", m_ints.toCopy);
  declareProperty("iparticlesToCopy", m_iparticles.toCopy);
}

StatusCode JetMatcherAlg::initialize() {
  std::vector<std::string> sources;
  for (const auto& key: m_sourceJets) {
    sources.emplace_back(key.key());
  }
  std::string target = m_targetJet.key();
  ATH_CHECK(m_floats.initialize(this, sources, target));
  ATH_CHECK(m_ints.initialize(this, sources, target));
  ATH_CHECK(m_iparticles.initialize(this, sources, target));
  m_drDecorator = target + "." + m_dRKey;
  m_dPtDecorator = target + "." + m_dPtKey;
  ATH_CHECK(m_targetJet.initialize());
  ATH_CHECK(m_sourceJets.initialize());
  ATH_CHECK(m_drDecorator.initialize());
  ATH_CHECK(m_dPtDecorator.initialize());
  // choose jet selection option
  using xAOD::Jet;
  if (float drMax = m_ptPriorityWithDeltaR.value(); drMax > 0) {
    m_jetSelector = [drMax](const Jet* tj, const JV& sv) -> const Jet* {
      // jets are already sorted by pt, so we can take the first match
      for (const auto* sj: sv) {
        if (tj->p4().DeltaR(sj->p4()) < drMax) return sj;
      }
      return nullptr;
    };
  } else {
    m_jetSelector = [](const Jet* tj, const JV& sv) -> const Jet* {
      std::vector<std::pair<float, const Jet*>> jets;
      for (const auto* sj: sv) {
        jets.emplace_back(tj->p4().DeltaR(sj->p4()), sj);
      }
      auto sItr = std::min_element(jets.begin(), jets.end());
      if (sItr == jets.end()) return nullptr;
      return sItr->second;
    };
  }
  return StatusCode::SUCCESS;
}

namespace {
  using JL = ElementLink<xAOD::JetContainer>;
  using JC = xAOD::JetContainer;
  auto descending_pt = [](auto* j1, auto* j2){
    return j1->pt() > j2->pt();
  };
  std::vector<const xAOD::Jet*> getJetVector(
    SG::ReadHandle<JC>& handle) {
    std::vector<const xAOD::Jet*> jets(handle->begin(), handle->end());
    std::sort(jets.begin(),jets.end(), descending_pt);
    return jets;
  }
}

StatusCode JetMatcherAlg::execute(const EventContext& cxt) const {
  SG::ReadHandle<JC> targetJetGet(m_targetJet, cxt);
  SG::WriteDecorHandle<JC,float> drDecorator(m_drDecorator, cxt);
  SG::WriteDecorHandle<JC,float> dPtDecorator(m_dPtDecorator, cxt);
  auto targetJets = getJetVector(targetJetGet);
  std::vector<const xAOD::Jet*> sourceJets;
  for (const auto& key: m_sourceJets) {
    SG::ReadHandle<JC> h(key, cxt);
    sourceJets.insert(sourceJets.end(), h->begin(), h->end());
  }
  std::sort(sourceJets.begin(), sourceJets.end(), descending_pt);
  std::vector<MatchedPair<JC>> matches;
  for (const xAOD::Jet* target: targetJets) {
    const xAOD::Jet* source = m_jetSelector(target, sourceJets);
    if (source) {
      drDecorator(*target) = target->p4().DeltaR(source->p4());
      dPtDecorator(*target) = target->pt() - source->pt();
      matches.push_back({source, target});
    } else {
      drDecorator(*target) = NAN;
      dPtDecorator(*target) = NAN;
      matches.push_back({nullptr, target});
    }
  }
  m_floats.copy(matches, cxt);
  m_ints.copy(matches, cxt);
  m_iparticles.copy(matches, cxt);

  return StatusCode::SUCCESS;
}
StatusCode JetMatcherAlg::finalize () {
  return StatusCode::SUCCESS;
}
