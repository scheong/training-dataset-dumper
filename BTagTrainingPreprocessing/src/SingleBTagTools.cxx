#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "BTagJetWriterConfig.hh"
#include "SubjetWriter.hh"
#include "SoftElectronSelector.hh"

#include "JetWriters/getJetLinkWriter.h"
#include "JetWriters/IJetLinkWriter.h"

#include "FlavorTagDiscriminants/GNN.h"
#include "FlavorTagDiscriminants/HbbTag.h"
#include "FlavorTagDiscriminants/HbbTagConfig.h"
#include "FlavorTagDiscriminants/AssociationEnums.h"
#include "FlavorTagDiscriminants/BTagMuonAugmenter.h"

#include "JetMomentTools/JetVertexTaggerTool.h"

#include "H5Cpp.h"

namespace {
  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }

  BTagJetWriterConfig jwConfig(const SingleBTagConfig& jobcfg) {
    BTagJetWriterConfig jet_cfg;

    jet_cfg.event = jobcfg.event_vars;
    jet_cfg.jet = jobcfg.jet_vars;
    jet_cfg.btagging = jobcfg.btagging_vars;

    jet_cfg.variable_maps.replace_with_defaults_checks
      = jobcfg.default_flag_mapping;
    jet_cfg.variable_maps.rename = {}; // please don't use this :(
    jet_cfg.n_jets_per_event = 0;
    jet_cfg.name = "jets";
    jet_cfg.btagging_link = jobcfg.btagging_link;
    jet_cfg.force_full_precision = jobcfg.force_full_precision;
    return jet_cfg;
  }

  SubjetWriterConfig subjetWriterConfig(const SubjetConfig& jobcfg) {
    SubjetWriterConfig subjet_cfg;
    subjet_cfg.write_kinematics_relative_to_parent = true;
    subjet_cfg.jet = jobcfg.jet_vars;
    subjet_cfg.btagging = jobcfg.btagging_vars;
    subjet_cfg.n_subjets_to_save = jobcfg.n_subjets_to_save;
    subjet_cfg.btagging_link = jobcfg.btagging_link;
    subjet_cfg.name = jobcfg.output_name;
    return subjet_cfg;
  }

  template<typename T>
  std::function<void(const T&)> makeGNNWrapper(
    const std::string& path,
    const std::map<std::string, std::string>& remapping,
    FlavorTagDiscriminants::TrackLinkType tlt,
    FlavorTagDiscriminants::FlipTagConfig ftc,
    std::shared_ptr<TimingStats> timing)
  {
    FlavorTagDiscriminants::GNN gnn(path, ftc, remapping, tlt);
    return [gnn, timing](const T& j) {
      auto start = Clock::now();
      gnn.decorate(j);
      auto stop = Clock::now();
      timing->update(stop - start);
    };
  }

  template<typename T>
  std::function<void(const T&)> makeXbbWrapper(
    const std::string& path,
    const std::map<std::string, std::string>& remapping,
    std::shared_ptr<TimingStats> timing)
  {
    FlavorTagDiscriminants::HbbTagConfig cfg(path);
    // need to hardcode subjet collection`name for now
    if (remapping.count(cfg.subjet_link_name)) {
          cfg.subjet_link_name = remapping.at(cfg.subjet_link_name);
    }
    // xbb needs to be copy constructable so create a shared_ptr to it
    auto xbb_ptr = std::make_shared<FlavorTagDiscriminants::HbbTag>(cfg);

    return [xbb_ptr, timing](const T& j) {
      auto start = Clock::now();
      xbb_ptr->decorate(j);
      auto stop = Clock::now();
      timing->update(stop - start);
    };
  }

  template <>
  std::function<void(const xAOD::BTagging&)> makeXbbWrapper<>( 
    const std::string&,
    const std::map<std::string, std::string>&,
    std::shared_ptr<TimingStats>)
  {
    throw std::logic_error("The Xbb tagger must be called on a jet");
  }


  template<typename T>
  std::function<void(const T&)> makeDL2Wrapper(
    const std::string& path,
    const std::map<std::string, std::string>& remapping,
    FlavorTagDiscriminants::TrackLinkType tlt,
    FlavorTagDiscriminants::FlipTagConfig ftc,
    std::shared_ptr<TimingStats> timing)
  {
    FlavorTagDiscriminants::DL2HighLevel dl2(path, ftc, remapping, tlt);
    return [dl2, timing](const T& j) {
      auto start = Clock::now();
      dl2.decorate(j);
      auto stop = Clock::now();
      timing->update(stop - start);
    };
  }

  template<typename T>
  std::pair<std::shared_ptr<TimingStats>, std::function<void(const T&)>> makeNNWrapper(
    const DL2Config& cfg,
    FlavorTagDiscriminants::TrackLinkType tlt)
  {
    using E = DL2Config::Engine;
    const auto& path = cfg.nn_file_path;
    const auto& ftc = cfg.flip_tag_config;
    auto timing = std::make_shared<TimingStats>();
    timing->name = path;
    switch (cfg.engine) {
      case E::DL2: return {timing, makeDL2Wrapper<T>(path, cfg.remapping, tlt, ftc, timing)};
      case E::GNN: return {timing, makeGNNWrapper<T>(path, cfg.remapping, tlt, ftc, timing)};
      case E::XBB: return {timing, makeXbbWrapper<T>(path, cfg.remapping, timing)};
      default: throw std::logic_error("unknown engine");
    }
  }
}

TrackToolkit::TrackToolkit(const TrackConfig& cfg, H5::Group& output):
  selector(cfg.selection, cfg.input_name),
  sorted(trackSort(cfg.sort_order, cfg.selection.ip_prefix)),
  writer(output, cfg.writer),
  n_tracks_decorator("n_" + cfg.writer.name)
{
}
TrackToolkit::TrackToolkit(TrackToolkit&&) = default;

// electron toolkit
SoftElectronToolkit::SoftElectronToolkit(const SoftElectronConfig& cfg, H5::Group& output):
  n_electrons_decorator("n_electrons"),
  m_dec_electrons_links("jet_electrons")
{
  selector.reset(new SoftElectronSelector(cfg.selection));
  writer = getJetLinkWriter(output, cfg.writer);
}
SoftElectronToolkit::SoftElectronToolkit(SoftElectronToolkit&&) = default;

void SoftElectronToolkit::select_and_write(const xAOD::Jet* uncalib_jet,
                                const xAOD::ElectronContainer* electrons) {

  auto selected_electrons = selector->get_electrons(*uncalib_jet, *electrons);
  std::vector<ElementLink<xAOD::IParticleContainer>> electrons_links;

  for (auto el: selected_electrons){
    ElementLink<xAOD::IParticleContainer> link;
    link.toContainedElement(*electrons, el);
    electrons_links.push_back(link);
  }
  // decorate jet with electrons links
  m_dec_electrons_links(*uncalib_jet) = electrons_links;
  // write jet
  writer->write(*uncalib_jet);
  n_electrons_decorator(*uncalib_jet) = selected_electrons.size();
}

SubjetToolkit::SubjetToolkit(const SubjetConfig &init_cfg, H5::Group& output)
    : cfg(init_cfg),
      subjet_acc(init_cfg.input_name),
      parent_acc("Parent"),
      jet_writer(output, subjetWriterConfig(cfg)),
      trkSubjetsDecorator(init_cfg, init_cfg.tracks_name),
      n_subjets("n_" + init_cfg.output_name) {}
std::vector<const xAOD::Jet*> SubjetToolkit::getSubjets(const xAOD::Jet* jet){
  const xAOD::Jet* parent_jet = *parent_acc(*jet);
  std::vector<const xAOD::Jet*> subjets;
  auto subjet_links = subjet_acc(*parent_jet);
  for (const auto& el : subjet_links) {
    const auto* sjet = dynamic_cast<const xAOD::Jet*>(*el);
    if (!sjet) throw std::logic_error("subjet is invalid");
    if (sjet->pt() < cfg.min_jet_pt ||
        std::abs(sjet->eta()) > cfg.max_abs_eta)
      continue;
    if (sjet->numConstituents() < cfg.num_const) continue;
    subjets.push_back(sjet);
  }
  std::sort(
      subjets.begin(), subjets.end(),
      [](const auto* j1, const auto* j2) { return j1->pt() > j2->pt(); });
  return subjets;
}


SingleBTagTools::Accessors::Accessors(const SingleBTagConfig& cfg):
  eventClean_looseBad("DFCommonJets_eventClean_LooseBad"),
  jvt("Jvt")
{
  if (cfg.btagging_link.empty())
    btaggingLink = [] (const xAOD::Jet&) { return nullptr; };
  else{
    SG::AuxElement::ConstAccessor<ElementLink<xAOD::BTaggingContainer>>a(cfg.btagging_link);
    btaggingLink = [a] (const xAOD::Jet & j) { return *a(j); };
  }
}

SingleBTagTools::SingleBTagTools(const SingleBTagConfig& jobcfg):
  calibration_tool(jobcfg.tool_prefix + "JetCalibrationTool"),
  cleaning_tool(jobcfg.tool_prefix + "JetCleaningTool",
                JetCleaningTool::LooseBad, false),
#ifndef DISABLE_JVT
  jvttool(nullptr),
#endif
  shallow_copier(jobcfg.btagging_link),
  muon_augmenter(nullptr),
  track_classifier(jobcfg.tool_prefix + "TrackClassifierTool"),
  acc(jobcfg)
{
  std::string pfx = jobcfg.tool_prefix;
  if (jobcfg.calibration){
    const JetCalibrationConfig& cfg = *jobcfg.calibration;
    JetCalibrationTool& jct = calibration_tool;
    check_rc( jct.setProperty("JetCollection", cfg.collection));
    check_rc( jct.setProperty("ConfigFile", cfg.configuration) );
    check_rc( jct.setProperty("CalibSequence", cfg.seq) );
    check_rc( jct.setProperty("CalibArea", cfg.area) );
    check_rc( jct.setProperty("IsData", false) );
    check_rc( jct.initialize() );
  }
  check_rc( cleaning_tool.setProperty("JetContainer",
                                      jobcfg.jet_collection) );
  check_rc( cleaning_tool.initialize() );

  float min_jvt = jobcfg.selection.minimum_jvt;
  bool use_jvt = !(std::isinf(min_jvt) && min_jvt < 0);
  if (use_jvt) {
    jvttool.reset(new JetVertexTaggerTool(pfx + "JetVertexTaggerTool"));
    check_rc( jvttool->setProperty("JetContainer",
                                   jobcfg.jet_collection) );
    check_rc( jvttool->initialize() );
  }

  if (jobcfg.decorate.soft_muon) {
    using FlavorTagDiscriminants::BTagMuonAugmenter;
    muon_augmenter.reset(new BTagMuonAugmenter(pfx + "MuonAug"));
  }

  if (!jobcfg.nntc.empty()) {
    FlavorTagDiscriminants::TrackClassifier& tct = track_classifier;
    check_rc( tct.setProperty("NNModelFilepath", jobcfg.nntc) );
    check_rc( tct.initialize() );
  }
  for (const auto& cfg: jobcfg.dl2_configs) {
    using FlavorTagDiscriminants::FlipTagConfig;
    using DL2 = FlavorTagDiscriminants::DL2HighLevel;
    using TLT = FlavorTagDiscriminants::TrackLinkType;
    const TLT tpl = TLT::TRACK_PARTICLE;
    const TLT ipl = TLT::IPARTICLE;
    std::string path = cfg.nn_file_path;
    if (cfg.where == DL2Config::Where::BTAG) {
      auto [timing, nn] = makeNNWrapper<xAOD::BTagging>(cfg, tpl);
      dl2s.push_back(nn);
      timings.push_back(timing);
    } else if (cfg.where == DL2Config::Where::JET) {
      auto [timing, nn] = makeNNWrapper<xAOD::Jet>(cfg, ipl);
      jet_nns.push_back(nn);
      timings.push_back(timing);
    }
  }

  // instantiate truth particle decorators
  for (const auto& cfg: jobcfg.truths) {
    if (cfg.selection) {
      jet_truth_associators.emplace_back(
        cfg.association_name, *cfg.selection);
    }
    if (!cfg.merge.empty()) {
      jet_truth_mergers.emplace_back(cfg.merge, cfg.association_name);
    }
    // add overlap checks
    if (cfg.overlap_dr > 0) {
      SG::AuxElement::ConstAccessor<JetTruthAssociator::PartLinks> acc(
        cfg.association_name);
      overlap_checks.emplace_back(
        [acc, dr=cfg.overlap_dr](const xAOD::Jet& j) {
          for (const JetTruthAssociator::PartLink& tpl: acc(j)) {
            if (!tpl.isValid()) {
              throw std::runtime_error("invalid particle link");
            }
            const xAOD::IParticle* part = *tpl;
            if (part->p4().DeltaR(j.p4()) < dr) return true;
          }
          return false;
        });
    }
    if (cfg.decorate) {
      std::string out_name;
      if (cfg.output) { out_name = cfg.output->name; }
      else { out_name = cfg.association_name; }
      jetTruthSummaryDecorators.emplace_back(cfg.association_name, out_name);
    }
  }
  if (jobcfg.hits) {
    hit_decorator.reset(new HitDecorator(jobcfg.hits->decorator));
  }
}
SingleBTagTools::~SingleBTagTools() = default;

SingleBTagOutputs::SingleBTagOutputs(const SingleBTagConfig& jobcfg,
                                     H5::Group& output):
  jet_writer(output, jwConfig(jobcfg))
{
  for (const SubjetConfig &cfg : jobcfg.subjet_configs) {
    subjets.emplace_back(cfg, output);
  }
  for (const TrackConfig& cfg: jobcfg.tracks) {
    tracks.emplace_back(cfg, output);
  }
  if (jobcfg.electrons) {
    electrons.reset(new SoftElectronToolkit(*jobcfg.electrons, output));
  }
  for (const TruthConfig& cfg: jobcfg.truths) {
    if (cfg.output) {
      const TruthOutputConfig& oc = *cfg.output;
      // TODO: TruthWriter needs a config struct
      truths.emplace_back(output, oc.n_to_save,
                          cfg.association_name, oc.name,
                          oc.sort_order);
    }
  }
  if (jobcfg.hits) {
    const HitConfig& hits = *jobcfg.hits;
    if (hits.writer.output_size > 0) {
      if (hits.origin == HitConfig::Origin::PrimaryVertex) {
        hits_pv.emplace_back(output, hits.writer);
      } else if (hits.origin == HitConfig::Origin::Beamspot) {
        hits_bs.emplace_back(output, hits.writer);
      } else {
        throw std::runtime_error("Unknown origin for hits");
      }
    }
  }


  if (jobcfg.flow) {
    flow = getJetLinkWriter(output, *jobcfg.flow);
  }
}

SingleBTagOutputs::~SingleBTagOutputs() = default;
